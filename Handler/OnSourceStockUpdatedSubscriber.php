<?php

namespace Central\MsiMessageBus\Handler;

use Central\MsiMessageBus\Helper\Config;
use Central\MsiMessageBus\Model\SourceStockManagement;
use Magento\InventoryMessageBus\Api\AggregateStockManagement\OnUpdatedSubscriberInterface;
use Magento\InventoryMessageBus\Api\Data\Event\OnAggregateStockUpdatedInterface;

/**
 * Class OnSourceStockUpdatedSubscriber
 * @package Central\MsiMessageBus\Handler
 */
class OnSourceStockUpdatedSubscriber implements OnUpdatedSubscriberInterface
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @var SourceStockManagement
     */
    private $sourceStockManagement;

    /**
     * OnSourceStockUpdatedSubscriber constructor.
     * @param Config $config
     * @param SourceStockManagement $sourceStockManagement
     */
    public function __construct(
        Config $config,
        SourceStockManagement $sourceStockManagement
    ) {
        $this->config = $config;
        $this->sourceStockManagement = $sourceStockManagement;
    }

    /**
     * @inheritdoc
     */
    public function onUpdated(OnAggregateStockUpdatedInterface $message)
    {
        if ($this->config->isEnable()) {
            $this->sourceStockManagement->update($message->getSnapshot());
        }
    }
}
