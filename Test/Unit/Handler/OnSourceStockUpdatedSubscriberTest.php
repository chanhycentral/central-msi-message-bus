<?php

namespace Central\MsiMessageBus\Test\Unit\Handler;

use Central\MsiMessageBus\Api\Data\MsiMessageBusInterface;
use Central\MsiMessageBus\Api\Data\MsiMessageBusSearchResultsInterface;
use Central\MsiMessageBus\Handler\OnSourceStockUpdatedSubscriber;
use Central\MsiMessageBus\Helper\Config;
use Central\MsiMessageBus\Model\MsiMessageBusRepository;
use Central\MsiMessageBus\Model\SourceStockManagement;
use Magento\Framework\Api\SearchCriteria;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Magento\InventoryApi\Api\Data\SourceItemInterface;
use Magento\InventoryApi\Api\Data\SourceItemInterfaceFactory;
use Magento\InventoryApi\Api\Data\SourceItemSearchResultsInterface;
use Magento\InventoryApi\Api\SourceItemRepositoryInterface;
use Magento\InventoryApi\Api\SourceItemsSaveInterface;
use Magento\InventoryMessageBus\Api\Data\AggregateSnapshotInterface;
use Magento\InventoryMessageBus\Api\Data\AggregateSnapshotSkuInterface;
use Magento\InventoryMessageBus\Api\Data\Event\OnAggregateStockUpdatedInterface;
use Mockery;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

/**
 * Class OnSourceStockUpdatedSubscriberTest
 * @package Central\MsiMessageBus\Test\Unit\Handler
 */
class OnSourceStockUpdatedSubscriberTest extends TestCase
{
    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * @var Config|\Mockery\LegacyMockInterface|\Mockery\MockInterface
     */
    private $config;

    /**
     * @var SourceStockManagement|\Mockery\LegacyMockInterface|\Mockery\MockInterface
     */
    private $sourceStockManagement;

    /**
     * @var OnAggregateStockUpdatedInterface|Mockery\LegacyMockInterface|Mockery\MockInterface
     */
    private $onAggregateStockUpdatedInterface;

    /**
     * @var AggregateSnapshotInterface|Mockery\LegacyMockInterface|Mockery\MockInterface
     */
    private $aggregateSnapshotInterface;

    /**
     * @var AggregateSnapshotSkuInterface|Mockery\LegacyMockInterface|Mockery\MockInterface
     */
    private $aggregateSnapshotSkuInterface;

    /**
     * @var SearchCriteriaBuilder|Mockery\LegacyMockInterface|Mockery\MockInterface
     */
    private $searchCriteriaBuilder;

    /**
     * @var SearchCriteria|Mockery\LegacyMockInterface|Mockery\MockInterface
     */
    private $searchCriteria;

    /**
     * @var MsiMessageBusRepository|Mockery\LegacyMockInterface|Mockery\MockInterface
     */
    private $msiMessageBusRepository;

    /**
     * @var MsiMessageBusSearchResultsInterface|Mockery\LegacyMockInterface|Mockery\MockInterface
     */
    private $msiMessageBusSearchResultsInterface;

    /**
     * @var MsiMessageBusInterface|Mockery\LegacyMockInterface|Mockery\MockInterface
     */
    private $msiMessageBusInterface;

    /**
     * @var SourceItemRepositoryInterface|Mockery\LegacyMockInterface|Mockery\MockInterface
     */
    private $sourceItemRepository;

    /**
     * @var SourceItemSearchResultsInterface|Mockery\LegacyMockInterface|Mockery\MockInterface
     */
    private $sourceItemSearchResultsInterface;

    /**
     * @var SourceItemInterface|Mockery\LegacyMockInterface|Mockery\MockInterface
     */
    private $sourceItemInterface;

    /**
     * @var SourceItemInterfaceFactory|Mockery\LegacyMockInterface|Mockery\MockInterface
     */
    private $sourceItemFactory;

    /**
     * @var SourceItemsSaveInterface|Mockery\LegacyMockInterface|Mockery\MockInterface
     */
    private $sourceItemsSaveCommand;

    /**
     * @var Mockery\LegacyMockInterface|Mockery\MockInterface|LoggerInterface
     */
    private $logger;

    /**
     * inheritDoc
     */
    protected function setUp()
    {
        parent::setUp();
        $this->objectManager = new ObjectManager($this);
        $this->config = \Mockery::mock(Config::class);
        $this->sourceStockManagement = \Mockery::mock(SourceStockManagement::class);
        $this->onAggregateStockUpdatedInterface = \Mockery::mock(OnAggregateStockUpdatedInterface::class);
        $this->aggregateSnapshotInterface = \Mockery::mock(AggregateSnapshotInterface::class);
        $this->aggregateSnapshotSkuInterface = \Mockery::mock(AggregateSnapshotSkuInterface::class);
        $this->searchCriteriaBuilder = \Mockery::mock(SearchCriteriaBuilder::class);
        $this->searchCriteria = \Mockery::mock(SearchCriteria::class);
        $this->msiMessageBusRepository = \Mockery::mock(MsiMessageBusRepository::class);
        $this->msiMessageBusSearchResultsInterface = \Mockery::mock(MsiMessageBusSearchResultsInterface::class);
        $this->msiMessageBusInterface = \Mockery::mock(MsiMessageBusInterface::class);
        $this->sourceItemRepository = \Mockery::mock(SourceItemRepositoryInterface::class);
        $this->sourceItemSearchResultsInterface = \Mockery::mock(SourceItemSearchResultsInterface::class);
        $this->sourceItemInterface = \Mockery::mock(SourceItemInterface::class);
        $this->sourceItemFactory = \Mockery::mock(SourceItemInterfaceFactory::class);
        $this->sourceItemsSaveCommand = \Mockery::mock(SourceItemsSaveInterface::class);
        $this->logger = \Mockery::mock(LoggerInterface::class);
    }

    /**
     * inheritDoc
     */
    protected function tearDown()
    {
        Mockery::close();
    }

    /**
     * @covers OnSourceStockUpdatedSubscriber::onUpdated
     */
    public function testOnUpdated()
    {
        $this->aggregateSnapshotSkuInterface->shouldReceive('getSku')
            ->andReturn('sku');
        $this->aggregateSnapshotSkuInterface->shouldReceive('setSku')
            ->andReturnSelf();
        $this->aggregateSnapshotSkuInterface->shouldReceive('getQuantity')
            ->andReturn(1);
        $this->aggregateSnapshotSkuInterface->shouldReceive('setQuantity')
            ->andReturnSelf();
        $this->aggregateSnapshotSkuInterface->shouldReceive('isUnlimited')
            ->andReturn(false);
        $this->aggregateSnapshotSkuInterface->shouldReceive('setUnlimited')
            ->andReturnSelf();

        $this->aggregateSnapshotInterface->shouldReceive('getAggregateId')
            ->andReturn('aggregate_id');
        $this->aggregateSnapshotInterface->shouldReceive('setAggregateId')
            ->andReturnSelf();
        $this->aggregateSnapshotInterface->shouldReceive('getMode')
            ->andReturn('mode');
        $this->aggregateSnapshotInterface->shouldReceive('setMode')
            ->andReturnSelf();
        $this->aggregateSnapshotInterface->shouldReceive('getCreatedOn')
            ->andReturn('created_on');
        $this->aggregateSnapshotInterface->shouldReceive('setCreatedOn')
            ->andReturnSelf();
        $this->aggregateSnapshotInterface->shouldReceive('getStock')
            ->andReturn([$this->aggregateSnapshotSkuInterface]);
        $this->aggregateSnapshotInterface->shouldReceive('setStock')
            ->andReturnSelf();

        $this->onAggregateStockUpdatedInterface->shouldReceive('getSnapshot')
            ->andReturn($this->aggregateSnapshotInterface);
        $this->onAggregateStockUpdatedInterface->shouldReceive('setSnapshot')
            ->andReturnSelf();

        $this->config->shouldReceive('isEnable')->andReturnTrue();

        $this->sourceStockManagement->shouldReceive('update');

        $this->searchCriteriaBuilder->shouldReceive('addFilter')
            ->andReturnSelf();
        $this->searchCriteriaBuilder->shouldReceive('create')
            ->andReturn($this->searchCriteria);

        $this->msiMessageBusInterface->shouldReceive('getAggregateId')
            ->andReturn('aggregate_id');
        $this->msiMessageBusInterface->shouldReceive('setAggregateId')
            ->andReturnSelf();
        $this->msiMessageBusInterface->shouldReceive('getSourceCode')
            ->andReturn('source_code');
        $this->msiMessageBusInterface->shouldReceive('setSourceCode')
            ->andReturnSelf();

        $this->msiMessageBusSearchResultsInterface->shouldReceive('getItems')
            ->andReturn([$this->msiMessageBusInterface]);

        $this->msiMessageBusRepository->shouldReceive('getList')
            ->andReturn($this->msiMessageBusSearchResultsInterface);

        $this->sourceItemInterface->shouldReceive('getSku')
            ->andReturn('sku');
        $this->sourceItemInterface->shouldReceive('setSku')
            ->andReturnSelf();
        $this->sourceItemInterface->shouldReceive('getSourceCode')
            ->andReturn('source_code');
        $this->sourceItemInterface->shouldReceive('setSourceCode')
            ->andReturnSelf();
        $this->sourceItemInterface->shouldReceive('getQuantity')
            ->andReturn(1);
        $this->sourceItemInterface->shouldReceive('setQuantity')
            ->andReturnSelf();
        $this->sourceItemInterface->shouldReceive('getStatus')
            ->andReturn(1);
        $this->sourceItemInterface->shouldReceive('setStatus')
            ->andReturnSelf();

        $this->sourceItemSearchResultsInterface->shouldReceive('getItems')
            ->andReturn([$this->sourceItemInterface]);
        $this->sourceItemRepository->shouldReceive('getList')
            ->andReturn($this->sourceItemSearchResultsInterface);

        $this->sourceItemFactory->shouldReceive('create')
            ->andReturn($this->sourceItemInterface);

        $this->sourceItemsSaveCommand->shouldReceive('execute');
        $this->logger->shouldReceive('error');

        $onSourceStockUpdatedSubscriber = $this->getSubjectUnderTest();
        $onSourceStockUpdatedSubscriber->onUpdated($this->onAggregateStockUpdatedInterface);
    }

    /**
     * @return object|OnSourceStockUpdatedSubscriber
     */
    private function getSubjectUnderTest()
    {
        return $this->objectManager->getObject(OnSourceStockUpdatedSubscriber::class, [
            'config' => $this->config,
            'sourceStockManagement' => $this->sourceStockManagement
        ]);
    }
}
