<?php
namespace Central\MsiMessageBus\Api;

/**
 * @api
 */
interface MsiMessageBusRepositoryInterface
{
    /**
     * Create
     *
     * @param \Central\MsiMessageBus\Api\Data\MsiMessageBusInterface $msiMessageBus
     * @return int
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(\Central\MsiMessageBus\Api\Data\MsiMessageBusInterface $msiMessageBus);

    /**
     * Get
     *
     * @param int $id
     * @return \Central\MsiMessageBus\Api\Data\MsiMessageBusInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($id);

    /**
     * Delete
     *
     * @param \Central\MsiMessageBus\Api\Data\MsiMessageBusInterface $msiMessageBus
     * @return bool Will returned True if deleted
     * @throws \Magento\Framework\Exception\StateException
     */
    public function delete(\Central\MsiMessageBus\Api\Data\MsiMessageBusInterface $msiMessageBus);

    /**
     * Delete by id
     *
     * @param int $msiMessageBusId
     * @return bool Will returned True if deleted
     * @throws \Magento\Framework\Exception\StateException
     */
    public function deleteById($msiMessageBusId);

    /**
     * Get list
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Central\MsiMessageBus\Api\Data\MsiMessageBusSearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);
}
