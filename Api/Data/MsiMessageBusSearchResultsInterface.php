<?php
namespace Central\MsiMessageBus\Api\Data;

/**
 * @api
 */
interface MsiMessageBusSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * Get attributes list.
     *
     * @return \Central\MsiMessageBus\Api\Data\MsiMessageBusInterface[]
     */
    public function getItems();

    /**
     * Set attributes list.
     *
     * @param \Central\MsiMessageBus\Api\Data\MsiMessageBusInterface[] $msiMessageBuss
     * @return $this
     */
    public function setItems(array $msiMessageBuss);
}
