<?php
namespace Central\MsiMessageBus\Api\Data;

/**
 * @api
 */
interface MsiMessageBusInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{
    /**#@+
     * Constants defined for keys of data array
     */
    const ENTITY_ID = 'entity_id';
    const AGGREGATE_ID = 'aggregate_id';
    const SOURCE_CODE = 'source_code';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    /**#@-*/

    /**
      * Get entity id
      *
      * @return int|null
      */
    public function getEntityId();

    /**
      * Set entity id
      *
      * @param int $value
      * @return $this
      */
    public function setEntityId($value);

    /**
      * Get aggregate id
      *
      * @return string|null
      */
    public function getAggregateId();

    /**
      * Set aggregate id
      *
      * @param string $value
      * @return $this
      */
    public function setAggregateId($value);

    /**
      * Get source code
      *
      * @return string|null
      */
    public function getSourceCode();

    /**
      * Set source code
      *
      * @param string $value
      * @return $this
      */
    public function setSourceCode($value);

    /**
      * Get created at
      *
      * @return string|null
      */
    public function getCreatedAt();

    /**
      * Set created at
      *
      * @param string $value
      * @return $this
      */
    public function setCreatedAt($value);

    /**
      * Get updated at
      *
      * @return string|null
      */
    public function getUpdatedAt();

    /**
      * Set updated at
      *
      * @param string $value
      * @return $this
      */
    public function setUpdatedAt($value);

    /**
     * Retrieve existing extension attributes object or create a new one.
     *
     * @return \Central\MsiMessageBus\Api\Data\MsiMessageBusExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     *
     * @param \Central\MsiMessageBus\Api\Data\MsiMessageBusExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(\Central\MsiMessageBus\Api\Data\MsiMessageBusExtensionInterface $extensionAttributes);
}
