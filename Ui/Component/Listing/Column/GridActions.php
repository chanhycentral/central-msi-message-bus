<?php
namespace Central\MsiMessageBus\Ui\Component\Listing\Column;

/**
 * Class GridActions
 */
class GridActions extends \Magento\Ui\Component\Listing\Columns\Column
{
    /**
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource["data"]["items"])) {
            foreach ($dataSource["data"]["items"] as & $item) {
                $name = $this->getData("name");
                $id = "X";
                if (isset($item["entity_id"])) {
                    $id = $item["entity_id"];
                }
                $item[$name]["view"] = [
                    "href"=>$this->getContext()->getUrl(
                        "central_msimessagebus_msimessagebuss/msimessagebus/edit",
                        ["entity_id"=>$id]
                    ),
                    "label"=>__("Edit")
                ];
            }
        }

        return $dataSource;
    }
}
