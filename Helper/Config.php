<?php

namespace Central\MsiMessageBus\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

/**
 * Class Config
 * @package Central\McomInventory\Helper
 */
class Config extends AbstractHelper
{
    const XML_MSI_MESSAGE_BUS_ENABLE = 'msi_message_bus/general/enable';

    /**
     * @return bool
     */
    public function isEnable()
    {
        return $this->scopeConfig->isSetFlag(self::XML_MSI_MESSAGE_BUS_ENABLE);
    }
}
