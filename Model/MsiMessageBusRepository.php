<?php
namespace Central\MsiMessageBus\Model;

use Magento\Framework\DB\Adapter\DuplicateException;
use Magento\Framework\Exception\AlreadyExistsException;

/**
 * Class MsiMessageBusRepository
 * @package Central\MsiMessageBus\Model
 */
class MsiMessageBusRepository implements \Central\MsiMessageBus\Api\MsiMessageBusRepositoryInterface
{
    /**
     * @var \Central\MsiMessageBus\Api\Data\MsiMessageBusInterfaceFactory
     */
    protected $msiMessageBusFactory;

    /**
     * @var \Central\MsiMessageBus\Model\ResourceModel\MsiMessageBus
     */
    protected $resourceModel;

    /**
     * @var \Central\MsiMessageBus\Model\ResourceModel\MsiMessageBus\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var \Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface
     */
    protected $collectionProcessorInterface;

    /**
     * @var \Central\MsiMessageBus\Api\Data\MsiMessageBusSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @var \Magento\Framework\Api\FilterBuilder
     */
    protected $filterBuilder;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * MsiMessageBusRepository constructor.
     * @param \Central\MsiMessageBus\Api\Data\MsiMessageBusInterfaceFactory $msiMessageBusFactory
     * @param ResourceModel\MsiMessageBus $resourceModel
     * @param ResourceModel\MsiMessageBus\CollectionFactory $collectionFactory
     * @param \Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface $collectionProcessorInterface
     * @param \Central\MsiMessageBus\Api\Data\MsiMessageBusSearchResultsInterfaceFactory $searchResultsFactory
     * @param \Magento\Framework\Api\FilterBuilder $filterBuilder
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        \Central\MsiMessageBus\Api\Data\MsiMessageBusInterfaceFactory $msiMessageBusFactory,
        \Central\MsiMessageBus\Model\ResourceModel\MsiMessageBus $resourceModel,
        \Central\MsiMessageBus\Model\ResourceModel\MsiMessageBus\CollectionFactory $collectionFactory,
        \Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface $collectionProcessorInterface,
        \Central\MsiMessageBus\Api\Data\MsiMessageBusSearchResultsInterfaceFactory $searchResultsFactory,
        \Magento\Framework\Api\FilterBuilder $filterBuilder,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->msiMessageBusFactory = $msiMessageBusFactory;
        $this->resourceModel = $resourceModel;
        $this->collectionFactory = $collectionFactory;
        $this->collectionProcessorInterface = $collectionProcessorInterface;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->filterBuilder = $filterBuilder;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * {@inheritdoc}
     * @throws DuplicateException
     */
    public function save(\Central\MsiMessageBus\Api\Data\MsiMessageBusInterface $msiMessageBus)
    {
        try {
            $this->resourceModel->save($msiMessageBus);
        } catch (AlreadyExistsException $e) {
            throw new DuplicateException($e->getMessage());
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(__('Unable to save item'));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function get($msiMessageBusId)
    {
        $msiMessageBus = $this->msiMessageBusFactory->create();
        $this->resourceModel->load($msiMessageBus, $msiMessageBusId);
        if (!$msiMessageBus->getId()) {
            throw new \Magento\Framework\Exception\NoSuchEntityException(__('Requested item doesn\'t exist'));
        }
        return $msiMessageBus;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(\Central\MsiMessageBus\Api\Data\MsiMessageBusInterface $msiMessageBus)
    {
        $id = $msiMessageBus->getId();
        try {
            $this->resourceModel->delete($msiMessageBus);
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\StateException(__('Unable to remove item %1', $id));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($msiMessageBusId)
    {
        $msiMessageBus = $this->get($msiMessageBusId);
        return $this->delete($msiMessageBus);
    }

    /**
     * {@inheritdoc}
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
    {
        /** @var \Central\MsiMessageBus\Model\ResourceModel\MsiMessageBus\Collection $collection */
        $collection = $this->collectionFactory->create();
        $this->collectionProcessorInterface->process($searchCriteria, $collection);

        /** @var \Central\MsiMessageBus\Api\Data\MsiMessageBusSearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }
}
