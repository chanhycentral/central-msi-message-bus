<?php
namespace Central\MsiMessageBus\Model;

/**
 * Class MsiMessageBus
 * @package Central\MsiMessageBus\Model
 */
class MsiMessageBus extends \Magento\Framework\Model\AbstractExtensibleModel implements \Central\MsiMessageBus\Api\Data\MsiMessageBusInterface
{
    /**
     * {@inheritdoc}
     */
    protected function _construct()
    {
        $this->_init('Central\MsiMessageBus\Model\ResourceModel\MsiMessageBus');
    }

    /**
    * {@inheritdoc}
    */
    public function getEntityId()
    {
        return $this->_getData(self::ENTITY_ID);
    }

    /**
    * {@inheritdoc}
    */
    public function setEntityId($value)
    {
        return $this->setData(self::ENTITY_ID, $value);
    }
    /**
    * {@inheritdoc}
    */
    public function getAggregateId()
    {
        return $this->_getData(self::AGGREGATE_ID);
    }

    /**
    * {@inheritdoc}
    */
    public function setAggregateId($value)
    {
        return $this->setData(self::AGGREGATE_ID, $value);
    }
    /**
    * {@inheritdoc}
    */
    public function getSourceCode()
    {
        return $this->_getData(self::SOURCE_CODE);
    }

    /**
    * {@inheritdoc}
    */
    public function setSourceCode($value)
    {
        return $this->setData(self::SOURCE_CODE, $value);
    }
    /**
    * {@inheritdoc}
    */
    public function getCreatedAt()
    {
        return $this->_getData(self::CREATED_AT);
    }

    /**
    * {@inheritdoc}
    */
    public function setCreatedAt($value)
    {
        return $this->setData(self::CREATED_AT, $value);
    }
    /**
    * {@inheritdoc}
    */
    public function getUpdatedAt()
    {
        return $this->_getData(self::UPDATED_AT);
    }

    /**
    * {@inheritdoc}
    */
    public function setUpdatedAt($value)
    {
        return $this->setData(self::UPDATED_AT, $value);
    }

    /**
     * {@inheritdoc}
     */
    public function getExtensionAttributes()
    {
        $extensionAttributes = $this->_getExtensionAttributes();
        if (!$extensionAttributes) {
            return $this->extensionAttributesFactory->create('Central\MsiMessageBus\Api\Data\MsiMessageBusInterface');
        }
        return $extensionAttributes;
    }

    /**
     * {@inheritdoc}
     */
    public function setExtensionAttributes(\Central\MsiMessageBus\Api\Data\MsiMessageBusExtensionInterface $extensionAttributes)
    {
        return $this->_setExtensionAttributes($extensionAttributes);
    }
}
