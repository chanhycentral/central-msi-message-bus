<?php

namespace Central\MsiMessageBus\Model\System\Config\Source;

use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Data\OptionSourceInterface;
use Magento\InventoryApi\Api\SourceRepositoryInterface;

/**
 * Class Source
 * @package Central\MsiMessageBus\Model\System\Config\Source
 */
class Source implements OptionSourceInterface
{
    /**
     * @var array
     */
    protected $_options;

    /**
     * @var SourceRepositoryInterface
     */
    private $sourceRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * Source constructor.
     * @param SourceRepositoryInterface $sourceRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        SourceRepositoryInterface $sourceRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->sourceRepository = $sourceRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * @return array
     */
    public function getOptionArray()
    {
        $optionArray = ['' => ' '];
        foreach ($this->toOptionArray() as $option) {
            $optionArray[$option['value']] = $option['label'];
        }
        return $optionArray;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        if (!$this->_options) {
            $this->_options = [];
            $searchCriteria = $this->searchCriteriaBuilder->create();
            $searchResult = $this->sourceRepository->getList($searchCriteria);
            $items = $searchResult->getItems();
            foreach ($items as $item) {
                $this->_options[] = ['value' => $item->getSourceCode(), 'label' => $item->getName()];
            }
        }
        return $this->_options;
    }
}
