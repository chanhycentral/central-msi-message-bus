<?php

namespace Central\MsiMessageBus\Model;

use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\LocalizedException;
use Magento\InventoryApi\Api\Data\SourceItemInterface;
use Magento\InventoryApi\Api\Data\SourceItemInterfaceFactory;
use Magento\InventoryApi\Api\SourceItemRepositoryInterface;
use Magento\InventoryApi\Api\SourceItemsSaveInterface;
use Magento\InventoryMessageBus\Api\Data\AggregateSnapshotInterface;
use Magento\InventoryMessageBus\Api\Data\AggregateSnapshotSkuInterface;
use Psr\Log\LoggerInterface;

/**
 * Class SourceStockManagement
 * @package Central\MsiMessageBus\Model
 */
class SourceStockManagement
{
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var SourceItemInterfaceFactory
     */
    private $sourceItemFactory;

    /**
     * @var SourceItemRepositoryInterface
     */
    private $sourceItemRepository;

    /**
     * @var SourceItemsSaveInterface
     */
    private $sourceItemsSaveCommand;

    /**
     * @var MsiMessageBusRepository
     */
    private $msiMessageBusRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * SourceStockManagement constructor.
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param SourceItemInterfaceFactory $sourceItemFactory
     * @param SourceItemRepositoryInterface $sourceItemRepository
     * @param SourceItemsSaveInterface $sourceItemsSaveCommand
     * @param \Central\MsiMessageBus\Model\MsiMessageBusRepository $msiMessageBusRepository
     * @param LoggerInterface $logger
     */
    public function __construct(
        SearchCriteriaBuilder $searchCriteriaBuilder,
        SourceItemInterfaceFactory $sourceItemFactory,
        SourceItemRepositoryInterface $sourceItemRepository,
        SourceItemsSaveInterface $sourceItemsSaveCommand,
        MsiMessageBusRepository $msiMessageBusRepository,
        LoggerInterface $logger
    ) {
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->sourceItemFactory = $sourceItemFactory;
        $this->sourceItemRepository = $sourceItemRepository;
        $this->sourceItemsSaveCommand = $sourceItemsSaveCommand;
        $this->msiMessageBusRepository = $msiMessageBusRepository;
        $this->logger = $logger;
    }

    /**
     * Update existing source items from source snapshot
     *
     * @param AggregateSnapshotInterface $snapshot
     */
    public function update(AggregateSnapshotInterface $snapshot)
    {
        $aggregateId = $snapshot->getAggregateId();
        $sourceCode = $this->getSourceCodeByAggregateId($aggregateId);
        if ($sourceCode) {
            $sourceItemsBySku = [];

            foreach ($this->getSourceItems($snapshot, $sourceCode) as $sourceItem) {
                $sourceItemsBySku[$sourceItem->getSku()] = $sourceItem;
            }

            foreach ($snapshot->getStock() as $sourceSnapshotSku) {
                $sku = $sourceSnapshotSku->getSku();

                $sourceItem = $sourceItemsBySku[$sku] ?? $this->createSourceItem($sourceCode, $sku);
                $sourceItemsBySku[$sku] = $sourceItem;

                $this->updateSourceItem($sourceItem, $sourceSnapshotSku);
            }

            try {
                $this->sourceItemsSaveCommand->execute(array_values($sourceItemsBySku));
            } catch (LocalizedException $e) {
                $this->logger->error($e->getMessage());
            }
        }
    }

    /**
     * @param string $aggregateId
     * @return string
     */
    private function getSourceCodeByAggregateId($aggregateId)
    {
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter('aggregate_id', $aggregateId)
            ->create();
        if ($items = $this->msiMessageBusRepository->getList($searchCriteria)->getItems()) {
            $item = reset($items);
            return $item->getSourceCode();
        }
        return '';
    }

    /**
     * Get source items list by a given snapshot
     *
     * @param AggregateSnapshotInterface $snapshot
     * @param string $sourceCode
     * @return SourceItemInterface[]
     */
    private function getSourceItems(AggregateSnapshotInterface $snapshot, $sourceCode)
    {
        $skus = array_map(function (AggregateSnapshotSkuInterface $snapshotSku) {
            return $snapshotSku->getSku();
        }, $snapshot->getStock());

        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter(SourceItemInterface::SOURCE_CODE, $sourceCode)
            ->addFilter(SourceItemInterface::SKU, $skus, 'in')
            ->create();

        return $this->sourceItemRepository->getList($searchCriteria)->getItems();
    }

    /**
     * Update source item from a given snapshot SKU
     *
     * @param SourceItemInterface $sourceItem
     * @param AggregateSnapshotSkuInterface $sourceSnapshotSku
     * @return void
     */
    private function updateSourceItem(SourceItemInterface $sourceItem, $sourceSnapshotSku)
    {
        $salableQuantity = $sourceSnapshotSku->getQuantity();
        if ($salableQuantity <= 0) {
            $salableQuantity = 0;
        }

        $status = $salableQuantity ? SourceItemInterface::STATUS_IN_STOCK : SourceItemInterface::STATUS_OUT_OF_STOCK;

        $sourceItem->setQuantity($salableQuantity);
        $sourceItem->setStatus($status);
    }

    /**
     * Create source item with given source code and sku
     *
     * @param string $sourceCode
     * @param string $sku
     * @return SourceItemInterface
     */
    private function createSourceItem($sourceCode, $sku)
    {
        /** @var SourceItemInterface $sourceItem */
        $sourceItem = $this->sourceItemFactory->create();

        $sourceItem->setSku($sku);
        $sourceItem->setSourceCode($sourceCode);
        $sourceItem->setStatus(SourceItemInterface::STATUS_OUT_OF_STOCK);
        $sourceItem->setQuantity(0);

        return $sourceItem;
    }
}
