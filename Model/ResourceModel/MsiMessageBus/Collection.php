<?php
namespace Central\MsiMessageBus\Model\ResourceModel\MsiMessageBus;

/**
 * MsiMessageBus collection
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * {@inheritdoc}
     */
    protected function _construct()
    {
        $this->_init('Central\MsiMessageBus\Model\MsiMessageBus', 'Central\MsiMessageBus\Model\ResourceModel\MsiMessageBus');
    }
}
