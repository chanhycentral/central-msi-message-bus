<?php
namespace Central\MsiMessageBus\Model\ResourceModel;

/**
 * Item resource model
 */
class MsiMessageBus extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * {@inheritdoc}
     */
    protected function _construct()
    {
        $this->_init('central_msi_message_bus', 'entity_id');
    }
}
