<?php
namespace Central\MsiMessageBus\Controller\Adminhtml\MsiMessageBus;

use Magento\Backend\App\Action;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\DB\Adapter\DuplicateException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Stdlib\DateTime;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;

/**
 * Class Save
 */
class Save extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Central\MsiMessageBus::ruleName';

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var \Central\MsiMessageBus\Model\MsiMessageBusRepository
     */
    protected $objectRepository;

    /**
     * @var TimezoneInterface
     */
    private $timezone;

    /**
     * Save constructor.
     * @param Action\Context $context
     * @param DataPersistorInterface $dataPersistor
     * @param \Central\MsiMessageBus\Model\MsiMessageBusRepository $objectRepository
     * @param TimezoneInterface $timezone
     */
    public function __construct(
        Action\Context $context,
        DataPersistorInterface $dataPersistor,
        \Central\MsiMessageBus\Model\MsiMessageBusRepository $objectRepository,
        TimezoneInterface $timezone
    ) {
        $this->dataPersistor    = $dataPersistor;
        $this->objectRepository  = $objectRepository;
        $this->timezone = $timezone;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Redirect|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            if (empty($data['entity_id'])) {
                $data['entity_id'] = null;
            }

            /** @var \Central\MsiMessageBus\Model\MsiMessageBus $model */
            $model = $this->_objectManager->create('Central\MsiMessageBus\Model\MsiMessageBus');

            $id = $this->getRequest()->getParam('entity_id');
            $now = $this->getNowString();
            if ($id) {
                $model = $this->objectRepository->get($id);
            } else {
                $data['created_at'] = $now;
            }
            $data['updated_at'] = $now;
            $model->setData($data);

            try {
                $this->objectRepository->save($model);
                if (!$model->getId()) {
                    throw new DuplicateException(__('Unique constraint violation found.'));
                }
                $this->messageManager->addSuccess(__('You saved the thing.'));
                $this->dataPersistor->clear('central_msimessagebus_msimessagebus');
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['entity_id' => $model->getEntityId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (DuplicateException $e) {
                $this->messageManager->addError(__('The data is duplicated, please try again. The Aggregate Id must be unique.'));
            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the data.'));
            }

            $this->dataPersistor->set('central_msimessagebus_msimessagebus', $data);
            if (!$model->getId()) {
                return $resultRedirect->setPath('*/*/new', ['_current' => true]);
            }
            return $resultRedirect->setPath('*/*/edit', ['entity_id' => $this->getRequest()->getParam('entity_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * @return string
     */
    private function getNowString()
    {
        $date = $this->timezone->date();
        return $date->format(DateTime::DATETIME_PHP_FORMAT);
    }
}
